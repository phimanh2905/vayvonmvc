﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(vayvonmvc.Startup))]
namespace vayvonmvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
