﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace vayvonmvc.Controllers
{
    public class HomeController : Controller
    {
        public const string PATH = "~/Views/Home/";
        public ActionResult Index()
        {
            return View(PATH + "Index.cshtml");
        }

        public ActionResult VayTheoLuongChuyenKhoan()
        {
            return View(PATH + "VayTheoLuongChuyenKhoan.cshtml");
        }

        public ActionResult VayTheoLuongTienMat()
        {
            return View(PATH + "VayTheoLuongTienMat.cshtml");
        }
        public ActionResult VayTheoHDBH()
        {
            return View(PATH + "VayTheoHDBH.cshtml");
        }

    }
}